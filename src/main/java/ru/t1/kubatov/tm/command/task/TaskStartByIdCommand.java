package ru.t1.kubatov.tm.command.task;

import ru.t1.kubatov.tm.enumerated.Status;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Start Task by ID.";

    public final static String NAME = "task-start-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.IN_PROGRESS);
    }

}
