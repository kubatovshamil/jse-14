package ru.t1.kubatov.tm.api.model;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

}
